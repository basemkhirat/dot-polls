@extends("admin::layouts.master")
@section("content")
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
            <h2>
                <i class="fa fa-pie-chart"></i>
                {{ trans("polls::polls.polls") }}
            </h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route("admin") }}">{{ trans("admin::common.admin") }}</a>
                </li>
                <li>
                    <a href="{{ route("admin.polls.show") }}">{{ trans("polls::polls.polls") }}
                        ({{ $polls->total() }})</a>
                </li>
            </ol>
        </div>
        <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12 text-right">
            <a href="{{ route("admin.polls.create") }}" class="btn btn-primary btn-labeled btn-main"> <span
                    class="btn-label icon fa fa-plus"></span> {{ trans("polls::polls.add_new") }}</a>
        </div>
    </div>

    <div class="wrapper wrapper-content fadeInRight">

        <div id="content-wrapper">
            @include("admin::partials.messages")
            <form action="" method="get" class="filter-form">
                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                <input type="hidden" name="per_page" value="{{ Request::get('per_page') }}"/>
                <input type="hidden" name="tag_id" value="{{ Request::get('tag_id') }}"/>
                <div class="row">
                    <div class="col-lg-4 col-md-4">
                        <div class="form-group">
                            <select name="sort" class="form-control chosen-select chosen-rtl">
                                <option
                                    value="title"
                                    @if ($sort == "title") selected='selected' @endif>{{ trans("polls::polls.attributes.title") }}</option>
                                <option
                                    value="votes"
                                    @if ($sort == "votes") selected='selected' @endif>{{ trans("polls::polls.attributes.votes") }}</option>
                            </select>
                            <select name="order" class="form-control chosen-select chosen-rtl">
                                <option
                                    value="DESC"
                                    @if ($order == "DESC") selected='selected' @endif>{{ trans("polls::polls.desc") }}</option>
                                <option
                                    value="ASC"
                                    @if ($order == "ASC") selected='selected' @endif>{{ trans("polls::polls.asc") }}</option>
                            </select>
                            <button type="submit"
                                    class="btn btn-primary">{{ trans("polls::polls.order") }}</button>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div class="form-group">
                            <select name="status" class="form-control chosen-select chosen-rtl">
                                <option value="">{{ trans("polls::polls.all") }}</option>
                                <option @if (Request::get("status") == "1") selected='selected' @endif
                                value="1">{{ trans("polls::polls.activated") }}</option>
                                <option @if (Request::get("status") == "0") selected='selected' @endif
                                value="0">{{ trans("polls::polls.deactivated") }}</option>
                            </select>

                            <button type="submit"
                                    class="btn btn-primary">{{ trans("polls::polls.filter") }}</button>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <form action="" method="get" class="search_form">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                            <div class="input-group">
                                <input name="q" value="{{ Request::get("q") }}" type="text"
                                       class=" form-control"
                                       placeholder="{{ trans("polls::polls.search_polls") }} ...">
                                <span class="input-group-btn">
                            <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                        </span>
                            </div>
                        </form>
                    </div>
                </div>
            </form>
            <form action="" method="post" class="action_form">
                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>
                            <i class="fa fa-pie-chart"></i>
                            {{ trans("polls::polls.polls") }}
                        </h5>
                    </div>
                    <div class="ibox-content">
                        @if (count($polls))
                            <div class="row">
                                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 action-box">
                                    <select name="action" class="form-control pull-left">
                                        <option value="-1"
                                                selected="selected">{{ trans("polls::polls.bulk_actions") }}</option>
                                        <option value="delete">{{ trans("polls::polls.delete") }}</option>
                                        <option value="activate">{{ trans("polls::polls.activate") }}</option>
                                        <option value="deactivate">{{ trans("polls::polls.deactivate") }}</option>
                                    </select>
                                    <button type="submit"
                                            class="btn btn-primary pull-right">{{ trans("polls::polls.apply") }}</button>
                                </div>
                                <div class="col-lg-6 col-md-4 hidden-sm hidden-xs">
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                                    <select class="form-control per_page_filter">
                                        <option value="" selected="selected">-- {{ trans("polls::polls.per_page") }}
                                            --
                                        </option>
                                        @foreach (array(10, 20, 30, 40) as $num)
                                            <option
                                                value="{{ $num }}"
                                                @if ($num == $per_page) selected="selected" @endif>{{ $num }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table cellpadding="0" cellspacing="0" border="0"
                                       class="table table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th style="width:35px"><input type="checkbox" class="i-checks check_all"
                                                                      name="ids[]"/>
                                        </th>

                                        <th>{{ trans("polls::polls.attributes.title") }}</th>
                                        <th>{{ trans("polls::polls.tags") }}</th>
                                        <th>{{ trans("polls::polls.attributes.status") }}</th>
                                        <th>{{ trans("polls::polls.actions") }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($polls as $poll)
                                        <tr>
                                            <td>
                                                <input type="checkbox" class="i-checks" name="id[]"
                                                       value="{{ $poll->id }}"/>
                                            </td>

                                            <td>
                                                <a data-toggle="tooltip" data-placement="bottom"
                                                   title="{{ trans("polls::polls.edit") }}" class="text-navy"
                                                   href="{{ route("admin.polls.edit", array("id" => $poll->id)) }}">
                                                    {{ $poll->title }}
                                                </a>

                                            </td>

                                            <td>
                                                @if ($poll->tags->count())
                                                    @foreach ($poll->tags as $tag)
                                                        <a href="?tag_id={{ $tag->id }}" class="text-navy">
                                                            <span class="badge badge-primary">{{ $tag->name }}</span>
                                                        </a>
                                                    @endforeach
                                                @else
                                                    -
                                                @endif
                                            </td>

                                            <td>
                                                @if ($poll->status)
                                                    <a data-toggle="tooltip" data-placement="bottom"
                                                       title="{{ trans("polls::polls.activated") }}" class="ask"
                                                       message="{{ trans('polls::polls.sure_deactivate') }}"
                                                       href="{{ URL::route("admin.polls.status", array("id" => $poll->id, "status" => 0)) }}">
                                                        <i class="fa fa-toggle-on text-success"></i>
                                                    </a>
                                                @else
                                                    <a data-toggle="tooltip" data-placement="bottom"
                                                       title="{{ trans("polls::polls.deactivated") }}" class="ask"
                                                       message="{{ trans('polls::polls.sure_activate') }}"
                                                       href="{{ URL::route("admin.polls.status", array("id" => $poll->id, "status" => 1)) }}">
                                                        <i class="fa fa-toggle-off text-danger"></i>
                                                    </a>
                                                @endif
                                            </td>

                                            <td class="center">
                                                <a data-toggle="tooltip" data-placement="bottom"
                                                   title="{{ trans("polls::polls.edit") }}"
                                                   href="{{ route("admin.polls.edit", array("id" => $poll->id)) }}">
                                                    <i class="fa fa-pencil text-navy"></i>
                                                </a>
                                                <a data-toggle="tooltip" data-placement="bottom"
                                                   title="{{ trans("polls::polls.delete") }}" class="delete_user ask"
                                                   message="{{ trans("polls::polls.sure_delete") }}"
                                                   href="{{ URL::route("admin.polls.delete", array("id" => $poll->id)) }}">
                                                    <i class="fa fa-times text-navy"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    {{ trans("polls::polls.page") }}{{ $polls->currentPage() }}{{ trans("polls::polls.of") }}{{ $polls->lastPage() }}
                                </div>
                                <div class="col-lg-12 text-center">
                                    {{ $polls->appends(Request::all())->render() }}
                                </div>
                            </div>
                        @else
                            {{ trans("polls::polls.no_records") }}
                        @endif
                    </div>
                </div>
            </form>
        </div>

    </div>

@stop

@section("footer")

    <script>

        $(document).ready(function () {

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $('.check_all').on('ifChecked', function (event) {
                $("input[type=checkbox]").each(function () {
                    $(this).iCheck('check');
                    $(this).change();
                });
            });

            $('.check_all').on('ifUnchecked', function (event) {
                $("input[type=checkbox]").each(function () {
                    $(this).iCheck('uncheck');
                    $(this).change();
                });
            });

            $(".filter-form input[name=per_page]").val($(".per_page_filter").val());

            $(".per_page_filter").change(function () {
                var base = $(this);
                var per_page = base.val();
                $(".filter-form input[name=per_page]").val(per_page);
                $(".filter-form").submit();
            });

        });

    </script>

@stop

