@extends("admin::layouts.master")

@section("content")

    <form action="" method="post">

        <div class="row wrapper border-bottom white-bg page-heading">

            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <h2>
                    <i class="fa fa-pie-chart"></i>
                    {{ $poll ? trans("polls::polls.edit") : trans("polls::polls.add_new") }}
                </h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route("admin") }}">{{ trans("admin::common.admin") }}</a>
                    </li>
                    <li>
                        <a href="{{ route("admin.polls.show") }}">{{ trans("polls::polls.polls") }}</a>
                    </li>
                    <li class="active">
                        <strong>
                            {{ $poll ? trans("polls::polls.edit") : trans("polls::polls.add_new") }}
                        </strong>
                    </li>
                </ol>
            </div>

            <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12 text-right">

                @if ($poll)
                    <a href="{{ route("admin.polls.create") }}"
                       class="btn btn-primary btn-labeled btn-main pull-right"> <span
                            class="btn-label icon fa fa-plus"></span>
                        {{ trans("polls::polls.add_new") }}</a>
                @endif

                <button type="submit" class="btn btn-flat btn-danger btn-main">
                    <i class="fa fa-download" aria-hidden="true"></i>
                    {{ trans("polls::polls.save_poll") }}
                </button>

            </div>
        </div>

        <div class="wrapper wrapper-content fadeInRight">

            @include("admin::partials.messages")

            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
            <input type="hidden" name="parent" value="0"/>
            <div class="row">
                <div class="col-md-8">
                    <div class="panel panel-default">
                        <div class="panel-body">

                            <div class="form-group">
                                <label for="input-title">{{ trans("polls::polls.attributes.title") }}</label>
                                <input name="title" type="text"
                                       value="{{ @Request::old("title", $poll->title) }}"
                                       class="form-control" id="input-title"
                                       placeholder="{{ trans("polls::polls.attributes.title") }}">
                            </div>

                            <div class="form-group">
                                <label class="poll-left">
                                    {{ trans("polls::polls.answers") }}
                                    &nbsp;
                                    <a class="label pull-right poll-add" href="">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                </label>
                                <div class="polls-area">

                                    @if (count($poll->answers))

                                        @foreach ($poll->answers as $answer)
                                            <div class="row poll-row">

                                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">
                                                    <input type="hidden" name="images[]"
                                                           value="{{ $answer->image ? $answer->image->id : 0 }}"/>
                                                    <img style="cursor: pointer;" width="50" height="50"
                                                         src="{{ $answer->image ? thumbnail($answer->image->path) : assets("admin::default/post.png") }}"
                                                         class="add-answer-image" rel="{{ str_random(9) }}"/>
                                                </div>

                                                <div class="input-group col-lg-10 col-md-10 col-sm-10 col-xs-8">

                                                    <input name="answers[]" type="text" value="{{ $answer->title }}"
                                                           class="form-control" placeholder="">
                                                    <span class="input-group-addon">
                                        <a class="poll-remove" href="">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </span>
                                                </div>

                                            </div>
                                        @endforeach
                                    @else

                                        <div class="row poll-row">

                                            <div class="col-md-2">
                                                <input type="hidden" name="images[]" value="0"/>
                                                <img style="cursor: pointer;" width="50" height="50"
                                                     src="{{ assets("admin::default/post.png") }}"
                                                     class="add-answer-image" rel="{{ str_random(9) }}"/>
                                            </div>

                                            <div class="input-group col-md-10">

                                                <input name="answers[]" type="text" value=""
                                                       class="form-control" placeholder="">
                                                <span class="input-group-addon">
                                        <a class="poll-remove" href="">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </span>
                                            </div>


                                        </div>


                                    @endif
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <div class="col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-check-square"></i>
                            {{ trans("polls::polls.poll_status") }}
                        </div>
                        <div class="panel-body">
                            <div class="form-group switch-row">
                                <label class="col-sm-9 control-label"
                                       for="input-status">{{ trans("polls::polls.attributes.status") }}</label>
                                <div class="col-sm-3">
                                    <input @if (@Request::old("status", $poll->status)) checked="checked" @endif
                                    type="checkbox" id="input-status" name="status" value="1"
                                           class="status-switcher switcher-sm">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-picture-o"></i>
                            {{ trans("polls::polls.add_image") }}
                            <a class="remove-post-image pull-right" href="javascript:void(0)">
                                <i class="fa fa-times text-navy"></i>
                            </a>
                        </div>
                        <div class="panel-body form-group">
                            <div class="row post-image-block">
                                <input type="hidden" name="image_id" class="post-image-id"
                                       value="{{ ($poll and $poll->image) ? $poll->image->id : 0 }}">
                                <a class="change-post-image label" href="javascript:void(0)">
                                    <i class="fa fa-pencil text-navy"></i>
                                    {{ trans("polls::polls.change_image") }}
                                </a>
                                <a class="post-image-preview" href="javascript:void(0)">
                                    <img width="100%" height="130px" class="post-image"
                                         src="{{ ($poll and @$poll->image->id != "") ? thumbnail(@$poll->image->path) : assets("admin::default/post.png") }}">
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-tags"></i>
                            {{ trans("polls::polls.add_tag") }}
                        </div>
                        <div class="panel-body">
                            <div class="form-group" style="position:relative">
                                <input type="hidden" name="tags" id="tags_names"
                                       value="{{ join(",", $poll_tags) }}">
                                <ul id="mytags"></ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

@stop


@section("head")

    <link href="{{ assets("admin::tagit") }}/jquery.tagit.css" rel="stylesheet" type="text/css">
    <link href="{{ assets("admin::tagit") }}/tagit.ui-zendesk.css" rel="stylesheet" type="text/css">
    <style>

        .poll-row {
            margin: 5px 0;
        }

    </style>

@stop

@section("footer")

    <script type="text/javascript" src="{{ assets("admin::tagit") }}/tag-it.js"></script>
    <script>

        $(document).ready(function () {


            var activate_file_manager = function (id) {

                $(".add-answer-image[rel=" + id + "]").filemanager({
                    types: "image",
                    done: function (result, base) {
                        if (result.length) {
                            var file = result[0];
                            base.attr("src", file.url);
                            base.parents(".poll-row").find("input").eq(0).val(file.id);
                        }
                    },
                    error: function (media_path) {
                        alert("s{{ trans("polls::polls.not_allowed_file") }}");
                    }
                });
            }


            $(".add-answer-image").each(function () {
                var random_key = $(this).attr("rel");
                activate_file_manager(random_key);
            });

            $(".poll-add").click(function () {

                var random_key = Math.floor((Math.random() * 9999999999) + 1);
                var html = `
                  <div class="row poll-row">

                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">
                            <input type="hidden" name="images[]" value="0" />
                            <img style="cursor: pointer;" width="50" height="50"
                                 src="{{ assets("admin::default/post.png") }}"
                                 class="add-answer-image" rel="` + random_key + `"/>
                        </div>

                        <div class="input-group col-lg-10 col-md-10 col-sm-10 col-xs-8">
                            <input name="answers[]" type="text" value="" class="form-control" placeholder="">
                            <span class="input-group-addon">
                                <a class="poll-remove" href="">
                                    <i class="fa fa-times"></i>
                                </a>
                            </span>
                        </div>

                    </div>
                  `;

                $(".polls-area").append(html);

                activate_file_manager(random_key);

                return false;
            });

            $("body").on("click", ".poll-remove", function () {

                if ($(".polls-area .poll-row").length <= 2) {
                    alert_box("{{ trans("polls::polls.min_two_answers") }}");
                } else {
                    $(this).parents(".poll-row").remove();
                }

                return false;
            });


            var elems = Array.prototype.slice.call(document.querySelectorAll('.status-switcher'));
            elems.forEach(function (html) {
                var switchery = new Switchery(html, {size: 'small'});
            });

            $(".change-post-image").filemanager({
                types: "image",
                done: function (result, base) {
                    if (result.length) {
                        var file = result[0];
                        base.parents(".post-image-block").find(".post-image-id").first().val(file.id);
                        base.parents(".post-image-block").find(".post-image").first().attr("src", file.thumbnail);
                    }
                },
                error: function (media_path) {
                    alert("{{ trans("polls::polls.not_allowed_file") }}");
                }
            });

            $(".remove-post-image").click(function () {
                var base = $(this);
                $(".post-image-id").first().val(0);
                $(".post-image").attr("src", "{{ assets("admin::default/post.png") }}");
            });

            $("#mytags").tagit({
                singleField: true,
                singleFieldNode: $('#tags_names'),
                allowSpaces: true,
                minLength: 2,
                placeholderText: "",
                removeConfirmation: true,
                tagSource: function (request, response) {
                    $.ajax({
                        url: "{{ route("admin.tags.search") }}",
                        data: {term: request.term},
                        dataType: "json",
                        success: function (data) {
                            response($.map(data, function (item) {
                                return {
                                    label: item.name,
                                    value: item.name
                                }
                            }));
                        }
                    });
                }
            });

        });
    </script>
@stop
