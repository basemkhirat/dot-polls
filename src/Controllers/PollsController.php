<?php

namespace Dot\Polls\Controllers;

use Dot\Platform\Facades\Action;
use Dot\Platform\Controller;
use Dot\Polls\Models\Poll;
use Auth;
use Redirect;
use Request;
use View;

/*
 * Class PollsController
 * @package Dot\Polls\Controllers
 */

class PollsController extends Controller
{

    /*
     * View payload
     * @var array
     */
    protected $data = [];


    /*
     * Show all posts
     * @return mixed
     */
    function index()
    {

        if (Request::isMethod("post")) {
            if (Request::filled("action")) {
                switch (Request::get("action")) {
                    case "delete":
                        return $this->delete();
                    case "activate":
                        return $this->status(1);
                    case "deactivate":
                        return $this->status(0);
                }
            }
        }

        $this->data["sort"] = (Request::filled("sort")) ? Request::get("sort") : "title";
        $this->data["order"] = (Request::filled("order")) ? Request::get("order") : "DESC";
        $this->data['per_page'] = (Request::filled("per_page")) ? Request::get("per_page") : NULL;

        $query = Poll::with('image', 'user', 'tags')
            ->where("parent", 0)
            ->orderBy($this->data["sort"], $this->data["order"]);

        if (Request::filled("tag_id")) {
            $query->whereHas("tags", function ($query) {
                $query->where("tags.id", Request::get("tag_id"));
            });
        }

        if (Request::filled("user_id")) {
            $query->whereHas("user", function ($query) {
                $query->where("users.id", Request::get("user_id"));
            });
        }

        if (Request::filled("status")) {
            $query->where("status", Request::get("status"));
        }

        if (Request::filled("q")) {
            $query->search(urldecode(Request::get("q")));
        }

        $this->data["polls"] = $query->paginate($this->data['per_page']);

        return View::make("polls::show", $this->data);
    }

    /*
     * Delete poll by id
     * @return mixed
     */
    public function delete()
    {

        $ids = Request::get("id");

        $ids = is_array($ids) ? $ids : [$ids];

        foreach ($ids as $ID) {

            $poll = Poll::findOrFail($ID);

            // Fire deleting action

            Action::fire("poll.deleting", $poll);

            $poll->tags()->detach();
            $poll->delete();

            // Fire deleted action

            Action::fire("poll.deleted", $poll);
        }

        return Redirect::back()->with("message", trans("polls::polls.events.deleted"));
    }

    /*
     * Activating / Deactivating poll by id
     * @param $status
     * @return mixed
     */
    public function status($status)
    {

        $ids = Request::get("id");

        $ids = is_array($ids) ? $ids : [$ids];

        foreach ($ids as $id) {

            Poll::where("id", "!=", $id)->update([
                "status" => 0
            ]);

            $poll = Poll::findOrFail($id);

            // Fire saving action

            Action::fire("poll.saving", $poll);

            $poll->status = $status;
            $poll->save();

            // Fire saved action

            Action::fire("poll.saved", $poll);
        }

        if ($status) {
            $message = trans("polls::polls.events.activated");
        } else {
            $message = trans("polls::polls.events.deactivated");
        }

        return Redirect::back()->with("message", $message);
    }

    /*
     * Create a new poll
     * @return mixed
     */
    public function create()
    {

        $poll = new Poll();

        if (Request::isMethod("post")) {

            $poll->parent = 0;
            $poll->title = Request::get('title');
            $poll->lang = app()->getLocale();
            $poll->image_id = Request::get('image_id', 0);
            $poll->user_id = Auth::user()->id;
            $poll->status = Request::get("status", 0);

            // Fire saving action

            Action::fire("poll.saving", $poll);

            if (count(Request::get("answers", [])) < 2) {
                return Redirect::back()->withErrors([trans('polls::polls.count_answer_error')])->withInput(Request::all());
            }

            if (!$poll->validate()) {
                return Redirect::back()->withErrors($poll->errors())->withInput(Request::all());
            }

            $poll->save();
            $poll->syncTags(Request::get("tags"));

            $answers_images = Request::get("images", []);

            // Saving answers

            Poll::where("parent", $poll->id)->delete();

            $i = 0;

            foreach (Request::get("answers", []) as $answer_title) {

                if ($answer_title == "") {
                    $i++;
                    continue;
                }

                $answer = new Poll();

                $answer->parent = $poll->id;
                $answer->title = $answer_title;
                $answer->lang = app()->getLocale();
                $answer->user_id = Auth::user()->id;
                $answer->image_id = $answers_images[$i];
                $answer->status = Request::get('status', 0);

                $answer->save();

                $i++;
            }

            // Fire saved action

            Action::fire("poll.saved", $poll);

            return Redirect::route("admin.polls.edit", array("id" => $poll->id))
                ->with("message", trans("polls::polls.events.created"));
        }

        $this->data["poll_tags"] = array();
        $this->data["poll"] = $poll;

        return View::make("polls::edit", $this->data);
    }

    /*
     * Edit poll by id
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {

        $poll = Poll::findOrFail($id);

        if (Request::isMethod("post")) {

            Poll::where("id", "!=", $id)->update([
                "status" => 0
            ]);

            $poll->parent = 0;
            $poll->title = Request::get('title');
            $poll->lang = app()->getLocale();
            $poll->image_id = Request::get('image_id', 0);
            $poll->status = Request::get("status", 0);

            // Fire saving action

            Action::fire("poll.saving", $poll);

            if (count(Request::get("answers", [])) < 2) {
                return Redirect::back()->withErrors([trans('polls::polls.count_answer_error')])->withInput(Request::all());
            }

            if (!$poll->validate()) {
                return Redirect::back()->withErrors($poll->errors())->withInput(Request::all());
            }

            $poll->save();
            $poll->syncTags(Request::get("tags"));

            $answers_images = Request::get("images", []);

            // Saving answers

            Poll::where("parent", $poll->id)->delete();

            $i = 0;

            foreach (Request::get("answers", []) as $answer_title) {

                if ($answer_title == "") {
                    $i++;
                    continue;
                }

                $answer = new Poll();

                $answer->parent = $poll->id;
                $answer->title = $answer_title;
                $answer->lang = app()->getLocale();
                $answer->user_id = Auth::user()->id;
                $answer->image_id = $answers_images[$i];
                $answer->status = Request::get('status', 0);

                $answer->save();

                $i++;
            }

            // Fire saved action

            Action::fire("poll.saved", $poll);

            return Redirect::route("admin.polls.edit", ["id" => $id])->with("message", trans("polls::polls.events.updated"));
        }

        $this->data["poll_tags"] = $poll->tags->pluck("name")->toArray();
        $this->data["poll"] = $poll;

        return View::make("polls::edit", $this->data);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function search()
    {
        if (Request::ajax()) {
            $q = trim(urldecode(Request::get("term")));
            $posts = Poll::with('image')->search($q)
                ->take(15)
                ->where(['status' => 1, 'parent' => 0])
                ->get()->toArray();
            return response()->json($posts);
        }
    }
}
