<?php

namespace Dot\Polls;

use Illuminate\Support\Facades\Auth;
use Navigation;

class Polls extends \Dot\Platform\Plugin
{

    /*
     * Plugin permissions
     * @var array
     */
    protected $permissions = [
        "manage"
    ];

    /*
     * Plugin bootstrap
     * Called in system boot
     */
    public function boot()
    {

        parent::boot();


        Navigation::menu("sidebar", function ($menu) {

            if (Auth::user()->can("polls.manage")) {

                $menu->item('polls', trans("polls::polls.module"), route("admin.polls.show"))
                    ->order(1)
                    ->icon("fa-pie-chart");

            }
        });

    }
}

