<?php
return [

    'module' => 'الإستفتاءات',
    'polls' => 'الإستفتاءات',
    'poll' => 'إستفتاء',
    'add_new' => 'أضف إستفتاء جديد',
    'edit' => 'تعديل الإستفتاء',
    'back_to_polls' => 'العودة إلي الإستفتاءات',
    'no_records' => 'لا توجد إستفتاءات',
    'save_poll' => "حفظ الإستفتاء",
    'search' => 'البحث',
    'search_polls' => 'البحث في الإستفتاءات',
    'per_page' => 'لكل صفحة',
    'bulk_actions' => 'إختر إمر',
    'delete' => 'حذف',
    'apply' => 'تطبيق ',
    'page' => 'الصفحة',
    'of' => 'من',
    'order' => 'ترتيب',
    'sort_by' => 'ترتيب حسب',
    'asc' => 'تصاعدي',
    'desc' => 'تنازلي',
    'actions' => 'الحدث',
    'filter' => 'عرض',
    'count_answer_error'=>'يجب انا يكون عدد الاجابات 3',
    'poll_status' => 'الحالة',
    'activate' => 'تفعيل',
    'activated' => 'مفعل',
    'all' => 'الكل',
    'deactivate' => 'إلغاء التفعيل',
    'deactivated' => 'غير مفعل',
    'sure_activate' => "هل تريد تفعيل الإستفتاء ؟",
    'sure_deactivate' => "هل تريد إلغاء تفعيل الإستفتاء ؟",
    'sure_delete' => 'هل تريد حذف الإستفتاء ؟',

    'add_image' => 'إضف صورة',
    'change_image' => 'تغيير الصورة',
    'not_allowed_file' => 'الملف غير مسموح به',
    'user' => 'العضو',
    'tags' => 'الوسوم',
    'add_tag' => 'إضف وسوم',

    "answers" => "الإجابات",
    "min_two_answers" => "لابد من إدخال إجابتين علي الأقل",

    'attributes' => [

        'parent' => 'Parent',
        'title' => 'العنوان',
        'slug' => 'Slug',
        'votes' => 'عدد الأصوات',
        'created_at' => 'تاريخ الإنشاء',
        'updated_at' => 'تاريخ التحديث',
        'status' => 'الحالة'
    ],
    "events" => [
        'created' => 'تم إنشاء الإستفتاء بنجاح',
        'updated' => 'تم تحديث الإستفتاء بنجاح',
        'deleted' => 'تم حذف الإستفتاء بنجاح',
        'activated' => 'تم تقعيل الإستفتاء بنجاح',
        'deactivated' => 'تم إلغاء تفعيل الإستفتاء بنجاح'
    ],

    "permissions" => [
        "manage" => "التحكم بالصلاحيات"
    ]

];
