<?php
return [

    'module' => 'polls',

    'polls' => 'Polls',
    'poll' => 'poll',
    'add_new' => 'Add New Poll',
    'edit' => 'Edit Poll',
    'back_to_polls' => 'Back To Polls',
    'no_records' => 'No Polls Found',
    'save_poll' => 'Save Poll',
    'search' => 'Search',
    'search_polls' => 'Search Polls',
    'per_page' => 'Per Page',
    'bulk_actions' => 'Bulk Actions',
    'delete' => 'Delete',
    'apply' => 'Save',
    'page' => 'Page',
    'of' => 'of',
    'order' => 'Order',
    'sort_by' => 'Sort by',
    'asc' => 'Ascending',
    'desc' => 'Descending',
    'actions' => 'Actions',
    'filter' => 'Filter',
    'count_answer_error'=>'You must insert 3 answers at least',
    'poll_status' => 'Poll Status',
    'activate' => 'Activate',
    'activated' => 'Activated',
    'all' => 'All',
    'deactivate' => 'Deactivate',
    'deactivated' => 'Deactivated',
    'sure_activate' => "Are you sure to activate poll ?",
    'sure_deactivate' => "Are you sure to deactivate poll ?",
    'sure_delete' => 'Are you sure to delete ?',

    'add_image' => 'Add image',
    'change_image' => 'change image',
    'not_allowed_file' => 'File is not allowed',
    'user' => 'User',
    'tags' => 'Tags',
    'add_tag' => 'Add tags',

    "answers" => "Answers",
    "min_two_answers" => "You must insert two answers minimium",

    'attributes' => [

        'parent' => 'Parent',
        'title' => 'Title',
        'slug' => 'Slug',
        'lang' => 'Lang',
        'votes' => 'Votes',
        'created_at' => 'Created date',
        'updated_at' => 'Updated date',
        'status' => 'Status'
    ],
    "events" => [
        'created' => 'Poll created successfully',
        'updated' => 'Poll updated successfully',
        'deleted' => 'Poll deleted successfully',
        'activated' => 'Poll activated successfully',
        'deactivated' => 'Poll deactivated successfully'
    ],

    "permissions" => [
        "manage" => "Manage permissions"
    ]

];
