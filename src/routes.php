<?php

/*
 * WEB
 */

Route::group([
    "prefix" => ADMIN,
    "middleware" => ['web', 'auth:backend', 'can:polls.manage'],
    "namespace" => "Dot\\Polls\\Controllers"
], function ($route) {
    $route->group(["prefix" => "polls"], function ($route) {
        $route->any('/', ["as" => "admin.polls.show", "uses" => "PollsController@index"]);
        $route->any('/create', ["as" => "admin.polls.create", "uses" => "PollsController@create"]);
        $route->any('/search', ["as" => "admin.polls.search", "uses" => "PollsController@search"]);
        $route->any('/{id}/edit', ["as" => "admin.polls.edit", "uses" => "PollsController@edit"]);
        $route->any('/delete', ["as" => "admin.polls.delete", "uses" => "PollsController@delete"]);
        $route->any('/{status}/status', ["as" => "admin.polls.status", "uses" => "PollsController@status"]);
    });
});
